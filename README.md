Проект - Project name
---

Краткое описание проекта - предметная область.

---

## Ссылки

- [Figma](https://www.figma.com/)
- [Репозиторий фронта](https://gitlab.com/)
- [Тестовый домен](https://rocketfirm.net/)
- [Боевой домен](https:/rocketfirm.net/)

---
## Tech Stack

### Frontend

### Backend

- [Yii 2](https://www.yiiframework.com/) – PHP Framework
- [MariaDB](https://mariadb.org/) - СУБД (версия 10.2.33)

---

## Команда проекта

✅ – активный, ⛔️ – не активный, ✴️ – временный

#### PM

- PM – [Telegram](https://telegram.me/PM) ✅

#### Designer

- Designer - [Telegram](https://telegram.me/Designer) ✅

#### Art-direction

- Art-direction – [Telegram](https://telegram.me/Art-direction) ✅

#### Frontend

- Frontend – [Telegram](https://telegram.me/Frontend), [GitLab](https://gitlab.com/Frontend) ✅

#### Backend

- Backend – [Telegram](https://telegram.me/Backend), [GitLab](https://gitlab.com/Backend) ✅

---

Установка Backend:
--

~~~
composer create-project rocketfirmcom/yii2-app-basic
~~~

Создать файлы из примеров и прописать подключение к бд: 
~~~
\config\console.php
\config\console_test.php
\config\test.php
\config\web.php
\web\index.php
\yii
\yii_test
~~~

Запустить миграции: 
~~~
php yii migrate
php yii_test migrate
~~~

Создать пользователя:
~~~
php yii user/create admin@sitename.kz admin PASSWORD admin
~~~

Запуск тестов (Перед запуском нужно создать тестовую бд и прописать её в \config\console_test.php):
~~~
vendor\bin\codecept.bat run
~~~