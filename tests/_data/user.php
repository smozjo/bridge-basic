<?php
return [
    'user1' => [
        'id' => 1,
        'username' => 'admin',
        'email' => 'admin@sitename.kz',
        'auth_key' => '3LvrkcZcXCMeoizm_QB6XrkcOiTReuQa',
        'password_hash' => '$2y$10$SNBUqdKLn8kxe6fwNeRjp.qBbrw6QZFNBgrHqSXMfCC202Gxdx2Oy',
        'updated_at' => '1601452430',
        'created_at' => '1601452430',
    ],
    'user2' => [
        'id' => 101,
        'username' => 'napoleon69',
        'email' => 'aileen.barton@heaneyschumm.com',
        'auth_key' => 'dZlXsVnIDgIzFgX4EduAqkEPuphhOh9q',
        'password_hash' => '$2y$13$kkgpvJ8lnjKo8RuoR30ay.RjDf15bMcHIF7Vz1zz/6viYG5xJExU6',
        'updated_at' => '1601452430',
        'created_at' => '1601452430',
    ],
];