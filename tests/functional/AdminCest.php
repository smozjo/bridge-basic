<?php

use app\tests\fixtures\AuthAssignmentFixture;
use app\tests\fixtures\AuthItemFixture;
use app\tests\fixtures\UserFixture;

class AdminCest
{
    const USER_ADMIN = 1; // администратор сайта


    /**
     * Load fixtures before db transaction begin
     * Called in _before()
     * @see \Codeception\Module\Yii2::_before()
     * @see \Codeception\Module\Yii2::loadFixtures()
     * @return array
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php',
            ],
            'auth_item' => [
                'class' => AuthItemFixture::class,
                'dataFile' => codecept_data_dir() . 'auth_item.php',
            ],
            'auth_assigment' => [
                'class' => AuthAssignmentFixture::class,
                'dataFile' => codecept_data_dir() . 'auth_assigment.php',
            ],
        ];
    }

    public function _before(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::USER_ADMIN);

    }

    public function openSettingsPage(\FunctionalTester $I)
    {
        $I->amOnPage(['/admin/settings/index']);
        $I->see('Настройки', 'h1');
    }

    public function openUserPage(\FunctionalTester $I)
    {
        $I->amOnPage(['/user/admin']);
        $I->see('Управление пользователями', 'h1');
    }

    public function openFilesPage(\FunctionalTester $I)
    {
        $I->amOnPage(['/admin/default/elfinder']);
        $I->see('Файловый менеджер', 'h1');
    }

    public function openTranslatinPage(\FunctionalTester $I)
    {
        $I->amOnPage(['/admin/i18n/default']);
        $I->see('Переводы', 'h1');
    }

    public function openMetaPage(\FunctionalTester $I)
    {
        $I->amOnPage(['/admin/meta-page']);
        $I->see('Мета-теги', 'h1');
    }
}
