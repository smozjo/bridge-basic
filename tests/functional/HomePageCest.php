<?php

class HomePageCest
{
    public function _before(\FunctionalTester $I)
    {
        $I->amOnPage(['/']);
    }

    public function openHomePage(\FunctionalTester $I)
    {
        $I->see('Congratulations!', 'h1');
    }
}
