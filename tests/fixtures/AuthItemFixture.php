<?php
namespace app\tests\fixtures;

use app\modules\user\models\AuthItem;
use yii\test\ActiveFixture;

class AuthItemFixture extends ActiveFixture
{
    public $modelClass = AuthItem::class;
}