<?php
namespace app\tests\fixtures;

use Da\User\Model\User;
use yii\test\ActiveFixture;

class UserFixture extends ActiveFixture
{
    public $modelClass = User::class;
    public $depends = [
        AuthItemFixture::class,
        AuthAssignmentFixture::class,
    ];
}