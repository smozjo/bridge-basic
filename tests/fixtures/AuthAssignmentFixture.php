<?php
namespace app\tests\fixtures;

use app\modules\user\models\AuthAssignment;
use yii\test\ActiveFixture;

class AuthAssignmentFixture extends ActiveFixture
{
    public $modelClass = AuthAssignment::class;
}