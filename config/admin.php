<?php

use Bridge\Core\BridgeModule;

return [
    'class' => BridgeModule::class,
    'allowedRoles' => [
        'admin',
    ],
    'modules' => [
    ],
    'composeMenu' => function ($user, $roles, $authManager) {
        if ($user->can('admin')) {
            return [
                [
                    'title' => \Yii::t('app', 'Настройки'),
                    'icon' => 'gear',
                    'items' => [
                        [
                            'title' => \Yii::t('app', 'Настройки'),
                            'url' => ['/admin/settings/index'],
                            'active' => ['module' => 'admin', 'controller' => 'settings'],
                            'icon' => 'gear',
                        ],
                        [
                            'title' => \Yii::t('app', 'Пользователи'),
                            'url' => ['/user/admin'],
                            'active' => ['module' => 'user'],
                            'icon' => 'users',
                        ],
                        [
                            'title' => \Yii::t('app', 'Файловый менеджер'),
                            'url' => ['/admin/default/elfinder'],
                            'active' => ['module' => 'admin', 'controller' => 'default', 'action' => 'elfinder'],
                            'icon' => 'file',
                        ],
                        [
                            'title' => \Yii::t('app', 'Переводы'),
                            'url' => ['/admin/i18n/default'],
                            'active' => ['module' => 'i18n', 'controller' => 'default'],
                            'icon' => 'globe',
                        ],
                        [
                            'title' => \Yii::t('app', 'Мета-теги'),
                            'url' => ['/admin/meta-page'],
                            'active' => ['module' => 'admin', 'controller' => 'meta-page'],
                            'icon' => 'tags',
                        ],
                    ],
                ],
            ];
        }
        return [];
    },
    'on beforeAction' => function(\yii\base\ActionEvent $event) {

        $isDashBoard = $event->action->controller->module->id === 'admin' &&
            $event->action->controller->id === 'default' &&
            $event->action->id === 'index';
        
        if(!$isDashBoard) {
            return null;
        }

        if(\Yii::$app->user->isGuest) {
            return null;
        }

        if(\Yii::$app->user->getIdentity()->hasRole('admin')) {
            header('Location: /admin/settings/index');
            exit;
        }
    },
];